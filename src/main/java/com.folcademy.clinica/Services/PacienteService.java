package com.folcademy.clinica.Services;



import com.folcademy.clinica.Excepcions.BadRequestException;
import com.folcademy.clinica.Excepcions.NotFoundException;
import com.folcademy.clinica.Model.DTO.PacienteDto;
import com.folcademy.clinica.Model.DTO.PacienteEnteroDto;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service("pacienteService")
public class PacienteService{
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper) {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
    }


    public List<PacienteDto> listarTodos() {
        if(pacienteRepository.findAll().isEmpty())
            throw new NotFoundException("La lista de pacientes esta vacia");
        return pacienteRepository.findAll().stream().map(pacienteMapper::entityToDto).collect(Collectors.toList());


    }

    public PacienteDto listarUno(Integer idPaciente) {
        if(!pacienteRepository.existsById(idPaciente))
            throw new NotFoundException("No existe el paciente");
        return pacienteRepository.findById(idPaciente).map(pacienteMapper::entityToDto).orElse(null);

    }



    public PacienteDto agregar(PacienteDto entity){
        if(entity.getDni()==null)
            throw new BadRequestException("No se puede agregar el paciente");
        entity.setId(null);
        return pacienteMapper.entityToDto(pacienteRepository.save(pacienteMapper.dtoToEntity(entity)));
    }


    public PacienteEnteroDto editar(Integer idPaciente, PacienteEnteroDto dto){
        if(!pacienteRepository.existsById(idPaciente))
            throw new NotFoundException(("No existe el paciente"));
        dto.setIdPaciente(idPaciente);
        return pacienteMapper.entityToEnteroDto(
                pacienteRepository.save(pacienteMapper.enteroDtoToEntity(
                        dto
                ))
        );
    }



    public boolean eliminar(Integer id){
        if(!pacienteRepository.existsById(id))
            throw new NotFoundException("No existe el paciente");
        pacienteRepository.deleteById(id);
        return true;
    }


}
