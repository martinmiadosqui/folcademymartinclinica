package com.folcademy.clinica.Services;

import com.folcademy.clinica.Excepcions.BadRequestException;
import com.folcademy.clinica.Excepcions.NotFoundException;
import com.folcademy.clinica.Model.DTO.MedicoDto;
import com.folcademy.clinica.Model.DTO.MedicoEnteroDto;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
    }

    public List<MedicoDto> listarTodos(){
        if(medicoRepository.findAll().isEmpty())
            throw new NotFoundException("La lista de medicos esta vacia");
        return medicoRepository.findAll().stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
    }

    public MedicoDto listarUno(Integer id) {
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("No existe el medico");
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);

    }

    public MedicoDto agregar(MedicoDto entity){
        if(entity.getConsulta()<0)
            throw new BadRequestException("No se puede agregar el medico");
        entity.setId(null);
        return medicoMapper.entityToDto(medicoRepository.save(medicoMapper.dtoToEntity(entity)));
    }

    public MedicoEnteroDto editar(Integer idMedico,MedicoEnteroDto dto){
        if(!medicoRepository.existsById(idMedico))
            throw new NotFoundException(("No existe el medico"));
        dto.setIdMedico(idMedico);
        return medicoMapper.entityToEnteroDto(
                medicoRepository.save(medicoMapper.enteroDtoToEntity(
                        dto
                ))
        );
    }


    public boolean eliminar(Integer id){
        if(!medicoRepository.existsById(id))
            throw new NotFoundException("No existe el medico");
        medicoRepository.deleteById(id);
        return true;
    }

}
