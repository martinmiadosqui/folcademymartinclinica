package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTO.TurnoDto;
import com.folcademy.clinica.Model.DTO.TurnoEnteroDto;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/turno")
public class TurnoController {
    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }
    //@PreAuthorize("hasAnyAuthority('getTurno','getTurnos')")
    @GetMapping(value= "")
    public ResponseEntity<List<TurnoDto>> listarTodo(){
        return ResponseEntity.ok().body(turnoService.listarTodos());
    }

    //@PreAuthorize("hasAnyAuthority('getTurno','getTurnos')")
    @GetMapping("/{idTurno}")
    public ResponseEntity<TurnoDto>  listarUno(@PathVariable(name="idTurno")int id){
        return ResponseEntity.ok(turnoService.listarUno(id));
    }

    //@PreAuthorize("hasAuthority('postTurno')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto entity){
        return ResponseEntity.ok(turnoService.agregar(entity));
    }

    //@PreAuthorize("hasAuthority('putTurno')")
    @PutMapping("{idTurno}")
    public ResponseEntity<TurnoEnteroDto> editar(@PathVariable(name = "idTurno") int id,
                                                  @RequestBody @Validated TurnoEnteroDto dto){
        return ResponseEntity.ok(turnoService.editar(id,dto));
    }

    //@PreAuthorize("hasAuthority('delTurno')")
    @DeleteMapping("{idTurno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idTurno") Integer id){
        return ResponseEntity.ok(turnoService.eliminar(id));
    }

}

