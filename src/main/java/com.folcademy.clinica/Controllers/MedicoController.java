package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.DTO.MedicoDto;
import com.folcademy.clinica.Model.DTO.MedicoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/medico")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    //@PreAuthorize("hasAnyAuthority('getMedico','getMedicos')")
    @GetMapping("")
    public ResponseEntity<List<MedicoDto>> listarTodo(){
        return ResponseEntity.ok(medicoService.listarTodos());
    }

    //@PreAuthorize("hasAnyAuthority('getMedico','getMedicos')")
    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDto>  listarUno(@PathVariable(name="idMedico")int id){
        return ResponseEntity.ok(medicoService.listarUno(id));
    }

    //@PreAuthorize("hasAuthority('postMedico')")
    @PostMapping("")
    public ResponseEntity<MedicoDto> agregar(@RequestBody @Validated MedicoDto dto){
        return ResponseEntity.ok(medicoService.agregar(dto));
    }

    //@PreAuthorize("hasAuthority('putMedico')")
    @PutMapping("{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody @Validated MedicoEnteroDto dto){
        return ResponseEntity.ok(medicoService.editar(id,dto));
    }

    //@PreAuthorize("hasAuthority('delMedico')")
    @DeleteMapping("{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") Integer id){
        return ResponseEntity.ok(medicoService.eliminar(id));
    }

}

