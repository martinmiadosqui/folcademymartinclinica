package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.DTO.PacienteDto;
import com.folcademy.clinica.Model.DTO.PacienteEnteroDto;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.http.ResponseEntity;
//import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/paciente")
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    //@PreAuthorize("hasAnyAuthority('getPaciente','getPacientes')")
    @GetMapping("")
    public ResponseEntity<List<PacienteDto>> listarTodo(){
        return ResponseEntity.ok(pacienteService.listarTodos());
    }

    //@PreAuthorize("hasAnyAuthority('getPaciente','getPacientes')")
    @GetMapping(value = "/idPaciente")
    public ResponseEntity<PacienteDto>  listarUno(@PathVariable(name="idPaciente")int id){
        return ResponseEntity.ok(pacienteService.listarUno(id));
    }

    //@PreAuthorize("hasAuthority('postPaciente')")
    @PostMapping("")
    public ResponseEntity<PacienteDto> agregar(@RequestBody @Validated PacienteDto entity){
        return ResponseEntity.ok(pacienteService.agregar(entity));
    }

    //@PreAuthorize("hasAuthority('putPaciente')")
    @PutMapping("{idPaciente}")
    public ResponseEntity<PacienteEnteroDto> editar(@PathVariable(name = "idPaciente") int id,
                                                    @RequestBody @Validated PacienteEnteroDto dto){
        return ResponseEntity.ok(pacienteService.editar(id,dto));
    }

    //@PreAuthorize("hasAuthority('delPaciente')")
    @DeleteMapping("{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") Integer id){
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }
}
