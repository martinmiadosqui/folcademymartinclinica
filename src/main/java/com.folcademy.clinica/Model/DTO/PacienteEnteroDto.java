package com.folcademy.clinica.Model.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteEnteroDto {
    public Integer idPaciente;

    public String dni;

    public String nombre = "";

    public String apellido = "";

    public String telefono ="";
}
