/*package com.folcademy.clinica.Model.Entities.Security;

import lombok.*;

import javax.persistence.*;
import java.security.Timestamp;

@Entity
@Table(name="authorities")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Authority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "deleted", columnDefinition = "TINYINT")
    private boolean deleted;
    @Column(name = "created")
    private Timestamp created;
    @Column(name = "modified")
    private Timestamp modified;
}
*/