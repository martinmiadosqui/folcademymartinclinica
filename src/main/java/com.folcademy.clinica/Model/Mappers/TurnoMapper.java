package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.DTO.TurnoDto;
import com.folcademy.clinica.Model.DTO.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TurnoMapper {
    public TurnoDto entityToDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdpaciente(),
                                ent.getIdmedico()

                        )
                )
                .orElse(new TurnoDto());
    }

    public Turno dtoToEntity(TurnoDto dto){
        Turno entity = new Turno();
        entity.setId(dto.getIdTurno());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());

        return entity;
    }

    public TurnoEnteroDto entityToEnteroDto(Turno entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoEnteroDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdpaciente(),
                                ent.getIdmedico()
                        )
                )
                .orElse(new TurnoEnteroDto());
    }

    public Turno enteroDtoToEntity(TurnoEnteroDto dto){
        Turno entity = new Turno();
        entity.setId(dto.getIdTurno());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdpaciente(dto.getIdpaciente());
        entity.setIdmedico(dto.getIdmedico());
        return entity;
    }
}
